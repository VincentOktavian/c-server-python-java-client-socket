//Example code: A simple server side code, which echos back the received message. 
//Handle multiple socket connections with select and fd_set on Linux 
#include <stdio.h> 
#include <string.h> //strlen 
#include <stdlib.h> 
#include <errno.h> 
#include <unistd.h> //close 
#include <arpa/inet.h> //close 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros 
#include <time.h>

#define TRUE 1 
#define FALSE 0 
#define PORT 8888 

int dayofweek(int d, int m, int y)
{
	return ((d += m < 3 ? y-- : y - 2, 23 * m / 9 + d + 4 + y / 4 - y / 100 + y / 400) % 7);
}


int main(int argc, char *argv[])
{
	int opt = TRUE;
	int master_socket, addrlen, new_socket, client_socket[30],
		max_clients = 30, activity, i, valread, sd;
	int max_sd;
	struct sockaddr_in address;

	char buffer[1025]; //data buffer of 1K 
	char target[10];
	int len;
	char resp[1025];
	//set of socket descriptors 
	fd_set readfds;
	

	// variables to store date and time components
	int hours, minutes, seconds, day, month, year;
	int weekday;
	// time_t is arithmetic time type
	time_t now;

	// Obtain current time
	// time() returns the current time of the system as a time_t value
	time(&now);

	// Convert to local time format and print to stdout
	printf("Today is : %s", ctime(&now));

	// localtime converts a time_t value to calendar time and 
	// returns a pointer to a tm structure with its members 
	// filled with the corresponding values
	struct tm *local = localtime(&now);

	hours = local->tm_hour;      	// get hours since midnight (0-23)
	minutes = local->tm_min;     	// get minutes passed after the hour (0-59)
	seconds = local->tm_sec;     	// get seconds passed after minute (0-59)

	day = local->tm_mday;        	// get day of month (1 to 31)
	month = local->tm_mon + 1;   	// get month of year (0 to 11)
	year = local->tm_year + 1900;	// get year since 1900


	//a message 
	char *message = "ECHO Daemon v1.0 \r\n";

	//initialise all client_socket[] to 0 so not checked 
	for (i = 0; i < max_clients; i++)
	{
		client_socket[i] = 0;
	}

	//create a master socket 
	if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
	{
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	//set master socket to allow multiple connections , 
	//this is just a good habit, it will work without this 
	if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,
		sizeof(opt)) < 0)
	{
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	//type of socket created 
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(PORT);

	//bind the socket to localhost port 8888 
	if (bind(master_socket, (struct sockaddr *)&address, sizeof(address)) < 0)
	{
		perror("bind failed");
		exit(EXIT_FAILURE);
	}
	printf("Listener on port %d \n", PORT);

	//try to specify maximum of 3 pending connections for the master socket 
	if (listen(master_socket, 3) < 0)
	{
		perror("listen");
		exit(EXIT_FAILURE);
	}

	//accept the incoming connection 
	addrlen = sizeof(address);
	puts("Waiting for connections ...");

	while (TRUE)
	{
		//clear the socket set 
		FD_ZERO(&readfds);

		//add master socket to set 
		FD_SET(master_socket, &readfds);
		max_sd = master_socket;

		//add child sockets to set 
		for (i = 0; i < max_clients; i++)
		{
			//socket descriptor 
			sd = client_socket[i];

			//if valid socket descriptor then add to read list 
			if (sd > 0)
				FD_SET(sd, &readfds);

			//highest file descriptor number, need it for the select function 
			if (sd > max_sd)
				max_sd = sd;
		}

		//wait for an activity on one of the sockets , timeout is NULL , 
		//so wait indefinitely 
		activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);

		if ((activity < 0) && (errno != EINTR))
		{
			printf("select error");
		}

		//If something happened on the master socket , 
		//then its an incoming connection 
		if (FD_ISSET(master_socket, &readfds))
		{
			if ((new_socket = accept(master_socket,
				(struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0)
			{
				perror("accept");
				exit(EXIT_FAILURE);
			}

			//inform user of socket number - used in send and receive commands 
			printf("New connection , socket fd is %d , ip is : %s , port : %d \n" , new_socket , inet_ntoa(address.sin_addr) , ntohs (address.sin_port));

			//send new connection greeting message 
			if (send(new_socket, message, strlen(message), 0) != strlen(message))
			{
				perror("send");
			}

			puts("Welcome message sent successfully");

			//add new socket to array of sockets 
			for (i = 0; i < max_clients; i++)
			{
				//if position is empty 
				if (client_socket[i] == 0)
				{
					client_socket[i] = new_socket;
					printf("Adding to list of sockets as %d\n", i);

					break;
				}
			}
		}

		//else its some IO operation on some other socket 
		for (i = 0; i < max_clients; i++)
		{
			sd = client_socket[i];

			if (FD_ISSET(sd, &readfds))
			{
				//Check if it was for closing , and also read the 
				//incoming message 
				if ((valread = read(sd, buffer, 1024)) == 0)
				{
					//Somebody disconnected , get his details and print 
					getpeername(sd, (struct sockaddr*)&address, \
						(socklen_t*)&addrlen);
					printf("Host disconnected , ip %s , port %d \n",
						inet_ntoa(address.sin_addr), ntohs(address.sin_port));

					//Close the socket and mark as 0 in list for reuse 
					close(sd);
					client_socket[i] = 0;
				}

				//Echo back the message that came in 
				else
				{
					//set the string terminating NULL byte on the end 
					//of the data read 
					buffer[valread] = '\0';
					snprintf(target, sizeof target, "%.4s", buffer);
					if (strcmp(target, "halo") == 0)
					{
						strcpy(resp, "halo juga!\r\n");
						len = strlen(resp);
						resp[len] = '\0';
					}
					else if (strcmp(target, "nama") == 0)
					{

						strcpy(resp, "nama saya Vincent Oktavian Kaulika (13216115)\r\n");
						len = strlen(resp);
						resp[len] = '\0';
					}
					else if (strcmp(target, "jam ") == 0)
					{
						snprintf(resp, sizeof resp, "Sekarang Jam %02d:%02d:%02d\r\n", hours+7, minutes, seconds);
						len = strlen(resp);
						resp[len] = '\0';
					}
					else if (strcmp(target, "ini ") == 0)
					{
						weekday = local->tm_wday;
						//printf("%d", weekday);
						switch (weekday)
						{
						case 1:
							strcpy(resp, "Senin\r\n");
							break;
						case 2:
							strcpy(resp, "Selasa\r\n");
							break;
						case 3:
							strcpy(resp, "Rabu\r\n");
							break;
						case 4:
							strcpy(resp, "Kamis\r\n");
							break;
						case 5:
							strcpy(resp, "Jumat\r\n");
							break;
						case 6:
							strcpy(resp, "Sabtu\r\n");
							break;
						case 7:
							strcpy(resp, "Minggu\r\n");
							break;
						}
						len = strlen(resp);
						resp[len] = '\0';
					}
					else if (strcmp(target, "tang") == 0)
					{
						snprintf(resp, sizeof resp, "Hari ini tanggal %02d/%02d/%02d\r\n", day, month, year);
						len = strlen(resp);
						resp[len] = '\0';
					}
					else if (strcmp(target, "teri") == 0)
					{
						strcpy(resp, "sama-sama\r\n");
						len = strlen(resp);
						resp[len] = '\0';
					}
					else if (strcmp(target, "pami") == 0)
					{
						strcpy(resp, "silakan\r\n");
						len = strlen(resp);
						resp[len] = '\0';
					}
					else
					{
						strcpy(resp, "tidak paham\r\n");
						len = strlen(resp);
						resp[len] = '\0';
					}
					send(sd, resp, strlen(resp), 0);
				}
			}
		}
	}

	return 0;
}
